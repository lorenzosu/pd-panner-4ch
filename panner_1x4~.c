/******************************************************************************
* panner_1x4~ - A MAX/MSP and Pd external for 1 to 4 channel spatialisation
* ==== Pd version ===
* version 1.0 09-2010
* Copyright (C) 2010 Conservatorio di Santa Cecilia EMuTeam
* web: www.emufest.org
* Pd version: Lorenzo Sutton - lorenzofsutton@gmail.com
*  
* panner_1x4~ is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* panner_1x4~ is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************************/

#include "m_pd.h"
#ifdef NT
  #pragma warning( disable : 4244 )
  #pragma warning( disable : 4305 )
#endif
#include <math.h>

#define NCHAN	4
#define LOPCF	10.     // Cutoff frequency (Hz) of the smoothing low-pass filter
#define GRAD	0.017453
#define BESTSEPARATION	5	// optimal separation value to get about -3dB
#define SEPARATIONDIFF	0.2f	// apply this difference so that user can set separation to 20 to get optimal separation

//  Calculate amplitude for a single channel
//  amp: amplitude
//  chan: fin[0..NCHAN-1] ([0..7] for 8 chans) fin is an element of x
//  x is a pointer to the structure t_panner defined below
//  called from the function amplitudes only
//#define AMPLITUDE(amp, chan)	x->amp=x->A0*pow(10.0,((0.5*cos(x->angstep*chan-g)-0.5 )*f)*0.2)
#define AMPLITUDE(amp, chan)	x->amp=x->A0*pow(10.0,((0.5*cos(x->angle[chan] -g)-0.5 )*f)*0.2)

//  Calculate 1st order filter for smothing amplitudes when parameters change
//  chctl: channel to smooth, fin[0..NCHAN-1] ([0..7] for 8 chans)
//  chzm1: this is the z^-1 delay element of the filter
//      z[0..NCHAN-1] z is an element of x
#define SMOOTHLOP(chctl, chzm1) x->chzm1=x->chctl+x->B1*x->chzm1

void *panner_class; // main class for the object

// array with presets for angles
// Angles are in degrees for simplicity but could be directly in radiants
float presetangles[3][4] =  {
                                {-45,45,135,-135},
                                {-30,30,110,-110},
                                {0,90,270,180}
                            }; // quad, ITU, diamond (for debug)
char presetNames [3][70] = {
                        {"Quad:     spl1: -45° ,spk2: +45°,  spk3: +135°, spk4: -135°"},
                        {"ITU:      spk1: -30°, spk2: +30°,  spk3: +110°, spk4: -110°"},
                        {"Diamond:  spl1:   0°, spk2: +90°,  spk3: +180°, spk4: +270°"}
                };
typedef struct _panner
{
    t_object x_obj; // header
    t_float x_f;    // default arg
    float fin[NCHAN];   // array holding NCHAN channels
	short preset;
    float angle[NCHAN];   // array holding NCHAN channels
    float z[NCHAN];     // array holding NCHAN delay elements for lowpass filter
    float A0;           // A0 coefficient for lowpass filter
    float B1;           // B1 coefficient for lowpass filter
    float angstep;      // angle step between speakers: depends on speaker configuration
    float sep;          // separation parameter
    float angoffset;	// offset for 0 grd = center
	float azimuth; 		// azimuth parameter
} t_panner;

// function declarations
void *panner_new (float sep, float azi, float speaker_config);
t_int *panner_perform(t_int *w);
void ft1_separation(t_panner *x, float g);
void ft2_azimuth(t_panner *x, float f);
void ft_preset (t_panner *x, float g);

void loadpreset(t_panner *x);

void panner_dsp(t_panner *x, t_signal **sp);
void panner_assist(t_panner *x, void *b, long m, long a, char *s);
void amplitudes(t_panner *x);
//smooth filtering
void  smz_set(t_panner *x, float cf);

void panner_1x4_tilde_setup (void)
{
    panner_class = class_new(gensym("panner_1x4~"), (t_newmethod)panner_new, 0,
    sizeof(t_panner), 0, A_DEFFLOAT,A_DEFFLOAT,A_DEFFLOAT, 0);
    CLASS_MAINSIGNALIN(panner_class, t_panner, x_f);
    class_addmethod(panner_class, (t_method)panner_dsp, gensym("dsp"), 0);
    class_addmethod(panner_class, (t_method)ft1_separation, gensym("ft1"), A_FLOAT, 0);
    class_addmethod(panner_class, (t_method)ft2_azimuth, gensym("ft2"), A_FLOAT, 0);
    class_addmethod(panner_class, (t_method)ft_preset, gensym("preset"), A_FLOAT, 0);
}

// Function called when the object is created
void *panner_new(float sep, float azi, float speaker_config)
{
    int i;
    t_panner *x = (t_panner *)pd_new(panner_class);
	// 2 float inlets
	inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("ft1"));
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("ft2"));
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("preset"));

    for (i = 0; i<NCHAN;i++)
    {
        outlet_new(&x->x_obj, gensym("signal"));
    }
    x->sep = sep;                   // set initial separation
    x->azimuth = azi;               // set initial azimuth
    smz_set	(x,LOPCF);              // calculate

    x->sep = BESTSEPARATION;        // set initial separation to 19.5
    x->azimuth = 0.0f;              // set initial azimuth to 0.0
    x->angoffset = 45.0f;			// set offset for 0 grad = center
	smz_set	(x,LOPCF);              // calculate coefficients for cutoff of 10.0 Hz
    x->angstep = (360.0/NCHAN*GRAD);  // Calculate speaker angle step
	for (i=0; i<NCHAN;i++)

	x->preset = speaker_config;
	loadpreset(x);
    amplitudes(x);                  // calculate amplitudes for each channel

    post ("Separation %f",sep);
    post ("Azimuth %f",azi);
    post ("Preset: %s",presetNames[x->preset]);

    return x;
}

// changes separation
void ft1_separation(t_panner *x,  float f)
{
    x->sep = f + SEPARATIONDIFF;
    amplitudes(x);
}

// changes azimuth
void ft2_azimuth (t_panner *x, float g)
{
    //x->azimuth = g+x->angoffset;
    x->azimuth = g;
    amplitudes(x);
}

// changes speaker preset
void ft_preset (t_panner *x, float g)
{
    if (g == 0 || g == 1 || g == 2) {
        x->preset = (int) g;
        loadpreset(x);
        amplitudes(x);
    }
    else
    {
        post ("panner_1x4~: Preset must be 0,1,2, you input %d", (int) g);
    }
}
// loads a speaker preset
void loadpreset(t_panner *x)
{
	int i;
	int pr = (int) x->preset;
	for(i=0;i<NCHAN;i++)
	{
		x->angle[i] = presetangles[pr][i] * GRAD;
	}
    post ("%s",presetNames[pr]);
}


// calculate amplitudes for each channel
void amplitudes(t_panner *x)
{

    double f,g;
    f= x->sep;
    g= x->azimuth * GRAD;

    AMPLITUDE (fin[0], 0);
    AMPLITUDE (fin[1], 1);
    AMPLITUDE (fin[3], 2);
    AMPLITUDE (fin[2], 3);
}

// perform function for panner
t_int *panner_perform(t_int *w)
{
    t_panner *x = (t_panner *)(w[1]); // object is first arg

    float *in1 = (float *)(w[2]);	// input is next
    float *out1 = (float *)(w[3]);	// followed by the 1-st output
    float *out2 = (float *)(w[4]);	// followed by the 2-st output
    float *out3 = (float *)(w[5]);	// followed by the 3-st output
    float *out4 = (float *)(w[6]);	// followed by the 4-st output


    long n = w[7];	//vector size
    float val;		//input signal value
    float smoothval;//smoothed control value

    //outputs calculation
    while (n--)
    {
        val = *in1++;
        smoothval = SMOOTHLOP(fin[0], z[0]);    *out1++ = smoothval * val;
        smoothval = SMOOTHLOP(fin[1], z[1]);    *out2++ = smoothval * val;
        smoothval = SMOOTHLOP(fin[2], z[2]);    *out3++ = smoothval * val;
        smoothval = SMOOTHLOP(fin[3], z[3]);    *out4++ = smoothval * val;

    }

    return w + 8; // enables 'chain', n values + 1
}

// dsp function for the panner
void panner_dsp(t_panner *x, t_signal **sp)
{
    dsp_add(panner_perform, 7, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[3]->s_vec, sp[4]->s_vec,sp[0]->s_n);
}

void smz_set(t_panner *x, float cf)
{
    double cutoff = cf;			//cut-off frequency (Hz)
    double sr = sys_getsr();    //sampling frequency
    double Pi = 3.14159265;
    double cosf = 2.0 - cos (2. * Pi * (cutoff / 2.0) / sr);
    double cb1  = cosf - sqrt(cosf * cosf - 1.0);
    x->A0 = (float)(1.0 - cb1);
    x->B1 = (float)cb1;
}
